inputYear = int(input("Please input a year below\n"))
if inputYear % 4 == 0 and inputYear % 100 != 0 or inputYear % 400 == 0:
    print(f"{inputYear} is a leap year.")
else:
    print(f"{inputYear} is not a leap year.")

inputRow = int(input("Enter number of rows. \n")) 
inputCol = int(input("Enter number of columns. \n")) 

x = 1
y = 1
symbol = ""
while x <= inputRow:
    while y <= inputCol:
        symbol += "*"
        y += 1
    print(symbol)
    x += 1
